const expect = require('chai').expect;
const describe = require('mocha').describe;
const it = require('mocha').it;

const elasticlunr = require('elasticlunr');
require('../node_modules/lunr-languages/lunr.stemmer.support.js')(elasticlunr);
require('../lunr.zh.js')(elasticlunr); // Chinese

describe('Chinese languages support for lunr/elasticlunr', function () {
  it('Create an index and search in Chinese', function () {
    const index = elasticlunr(function () {
      // use the language (zh)
      this.use(elasticlunr.zh);
      // then, the normal elasticlunr index initialization
      this.addField('title');
      this.addField('body');
      this.setRef('id');
    });

    const doc1 = {
      'id': 1,
      'title': '超簡單、不失敗的鑄鐵鍋麵包',
      'body': '鑄鐵鍋厚實的鍋身、特殊外形的鍋蓋、鍋內底部的凹凸紋路，能溫和平均地傳導熱度，加上良好的保溫效果，能烹調出美味的三餐，因此讓人躍躍欲試。令人驚喜的還有，溫和的熱傳導、絕佳的保溫性也很適合麵團的發酵'
    };

    const doc2 = {
      'id': 2,
      'title': '我可以心甘情願，但你不能理所當然',
      'body': '他說：「老婆為我懷孕，那我替她結紮，其實想想，也挺浪漫的。既然夫妻雙方都有節育的共識，那不用多加思考，結紮，當然是男人結。因為憑什麼懷孕、生產，都已經由老婆代替我去受苦了，現在連「避孕」都要讓她代替我去？'
    };

    const doc3 = {
      'id': 3,
      'title': 'Bonjour',
      'body': 'Ce text est en français'
    };

    index.addDoc(doc1);
    index.addDoc(doc2);
    index.addDoc(doc3);

    const result = index.search('鑄鐵鍋厚實的鍋身、特殊外形的鍋蓋、鍋內底部的凹凸紋路');

    expect(result).to.not.equal(null);
    expect(result).to.be.an('array');
    expect(result).to.have.length(1);
  });
});
